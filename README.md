# Teste Java Maptriz

Este repositório contém uma atividade de teste para avaliar os conhecimentos de Java dos candidatos à desenvolvedor na Maptriz.

O teste consiste em:

- Criar uma API REST para gerenciar Pessoas (Físicas e Jurídicas) e deverá incluir as seguintes operações: Cadastrar, Atualizar, Recuperar e Remover.

# Detalhes da Modelagem

- Existem dois tipos de Pessoa: Física e Jurídica.
- Pessoas físicas possuem as seguintes informações: Nome, CPF e Data de Nascimento.
- Pessoas jurídicas possuem as seguintes informações: Nome Fantasia e CNPJ.
- As Pessoas têm as seguintes informações em comum: Telefone, E-mail e Endereço.
- Uma Pessoa pode ter um ou mais Endereços.

# Critérios de Avaliação

- Boas práticas de programação
- Estrutura da API RESTful
- Modelagem de Dados
- Conhecimento da Linguagem Java
- Conhecimento dos frameworks

# Diferenciais

- Testes Unitários e de Integração
- Utilização de Streams/Lambdas (Java 8)
- Organização do projeto
- Conceitos de Orientação à Objetos

# Envio

- Crie um fork desse repositório e envie suas modificações.
